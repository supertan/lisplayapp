package com.lisplay;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(SplashActivity splashActivity);

    void inject(LoginSignupActivity loginSignupActivity);

    void inject(WelcomeActivity welcomeActivity);

    void inject(EditProfileActivity editProfileActivity);

    void inject(CreatorDetailActivity creatorDetailActivity);

    void inject(UserProfileActivity userProfileActivity);

    void inject(MainActivity mainActivity);

    void inject(ChatActivity chatActivity);

    void inject(MemberChatActivity memberChatActivity);

    void inject(CreatorProfileActivity creatorProfileActivity);

    void inject(SubscriptionPaymentActivity subscriptionPaymentActivity);

    void inject(DashboardActivity dashboardActivity);

    void inject(ExploreFragment exploreFragment);

    void inject(MembershipFragment membershipFragment);

    void inject(ProfileFragment profileFragment);
}