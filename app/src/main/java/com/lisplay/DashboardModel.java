package com.lisplay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DashboardModel {

    @SerializedName("this_month_revenue")
    @Expose
    private Integer thisMonthRevenue;
    @SerializedName("left_this_month")
    @Expose
    private Integer leftThisMonth;
    @SerializedName("current_subscribers_count")
    @Expose
    private Integer currentSubscribersCount;
    @SerializedName("joined_this_month")
    @Expose
    private Integer joinedThisMonth;
    @SerializedName("subscribers_active")
    @Expose
    private List<SubscribersActive> subscribersActive = null;
    @SerializedName("subscribers_cancelled")
    @Expose
    private List<SubscribersActive> subscribersCancelled = null;
    @SerializedName("subscriptions_active")
    @Expose
    private List<SubscribersActive> subscriptionsActive = null;
    @SerializedName("subscriptions_cancelled")
    @Expose
    private List<SubscribersActive> subscriptionsCancelled = null;

    public Integer getThisMonthRevenue() {
        return thisMonthRevenue;
    }

    public void setThisMonthRevenue(Integer thisMonthRevenue) {
        this.thisMonthRevenue = thisMonthRevenue;
    }

    public Integer getLeftThisMonth() {
        return leftThisMonth;
    }

    public void setLeftThisMonth(Integer leftThisMonth) {
        this.leftThisMonth = leftThisMonth;
    }

    public List<SubscribersActive> getSubscribersActive() {
        return subscribersActive;
    }

    public void setSubscribersActive(List<SubscribersActive> subscribersActive) {
        this.subscribersActive = subscribersActive;
    }

    public Integer getCurrentSubscribersCount() {
        return currentSubscribersCount;
    }

    public void setCurrentSubscribersCount(Integer currentSubscribersCount) {
        this.currentSubscribersCount = currentSubscribersCount;
    }

    public Integer getJoinedThisMonth() {
        return joinedThisMonth;
    }

    public void setJoinedThisMonth(Integer joinedThisMonth) {
        this.joinedThisMonth = joinedThisMonth;
    }

    public List<SubscribersActive> getSubscribersCancelled() {
        return subscribersCancelled;
    }

    public void setSubscribersCancelled(List<SubscribersActive> subscribersCancelled) {
        this.subscribersCancelled = subscribersCancelled;
    }


    public List<SubscribersActive> getSubscriptionsActive() {
        return subscriptionsActive;
    }

    public void setSubscriptionsActive(List<SubscribersActive> subscriptionsActive) {
        this.subscriptionsActive = subscriptionsActive;
    }

    public List<SubscribersActive> getSubscriptionsCancelled() {
        return subscriptionsCancelled;
    }

    public void setSubscriptionsCancelled(List<SubscribersActive> subscriptionsCancelled) {
        this.subscriptionsCancelled = subscriptionsCancelled;
    }

    public class SubscribersActive {

        @SerializedName("subscription_id")
        @Expose
        private String subscriptionId;
        @SerializedName("plan")
        @Expose
        private Integer plan;
        @SerializedName("subscriber")
        @Expose
        private UserModel.User subscriber;
        @SerializedName("creator")
        @Expose
        private UserModel.User creator;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("paid_count")
        @Expose
        private Integer paidCount;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public String getSubscriptionId() {
            return subscriptionId;
        }

        public void setSubscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
        }

        public Integer getPlan() {
            return plan;
        }

        public void setPlan(Integer plan) {
            this.plan = plan;
        }

        public UserModel.User getSubscriber() {
            return subscriber;
        }

        public void setSubscriber(UserModel.User subscriber) {
            this.subscriber = subscriber;
        }

        public UserModel.User getCreator() {
            return creator;
        }

        public void setCreator(UserModel.User creator) {
            this.creator = creator;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public Integer getPaidCount() {
            return paidCount;
        }

        public void setPaidCount(Integer paidCount) {
            this.paidCount = paidCount;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }
}
