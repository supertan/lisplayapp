package com.lisplay;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


public class CreatorProfileAdapter extends RecyclerView.Adapter<CreatorProfileAdapter.ItemViewHolder> {
    private List<UserModel.User> creators;
    private Activity activity;

    public CreatorProfileAdapter(Activity activity, List<UserModel.User> items) {
        this.activity = activity;
        this.creators = items;
    }

    @NonNull
    @Override
    public CreatorProfileAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_creator_profile, null);
        return new CreatorProfileAdapter.ItemViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CreatorProfileAdapter.ItemViewHolder holder, final int position) {
        holder.fullName.setText(creators.get(position).getFirstName());
        holder.shortBio.setText(creators.get(position).getShortBio());

        Glide.with(activity)
                .load(creators.get(position).getPicture())
                .centerCrop()
                .placeholder(R.drawable.profile_image_placeholder)
                .error(R.drawable.profile_image_placeholder)
                .override(500, 500)
                .into(holder.profilePicture);

        holder.profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, CreatorProfileActivity.class);
                intent.putExtra("username", creators.get(holder.getAdapterPosition()).getUsername());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return creators.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        LinearLayout profileLayout;
        ImageView profilePicture;
        TextView fullName, shortBio;

        public ItemViewHolder(View itemView) {
            super(itemView);
            profileLayout = itemView.findViewById(R.id.creator_profile_layout);
            profilePicture = itemView.findViewById(R.id.creator_profile_picture);
            fullName = itemView.findViewById(R.id.creator_first_name);
            shortBio = itemView.findViewById(R.id.creator_short_bio);
        }
    }
}

