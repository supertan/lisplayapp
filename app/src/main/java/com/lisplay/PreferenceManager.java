package com.lisplay;


import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferenceManager {
    private static final String PREF = "lisplay";
    private static final String TOKEN = "token";
    private static final String APP_STAGE = "app_stage";
    private static final String IS_CREATOR = "is_creator";
    private static final String USERNAME = "username";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_MOBILE = "user_mobile";
    private static final String USER_FIRST_NAME = "user_first_name";
    private static final String USER_SHORT_BIO = "user_short_bio";
    private static final String USER_OBJECT = "user_object";


    private SharedPreferences sharedPreferences;

    @Inject
    public PreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }

    private SharedPreferences getSharedPref() {
        return sharedPreferences;
    }

    public String getToken() {
        return getSharedPref().getString(TOKEN, "");
    }

    public void setToken(String token) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(TOKEN, token);
        edit.apply();
    }

    public boolean isCreator() {
        return getSharedPref().getBoolean(IS_CREATOR, false);
    }

    public void setIsCreator(boolean isCreator) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putBoolean(IS_CREATOR, isCreator);
        edit.apply();
    }

    public int getAppStage() {
        return getSharedPref().getInt(APP_STAGE, 0);
    }

    public void setAppStage(int appStage) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putInt(APP_STAGE, appStage);
        edit.apply();
    }

    public String getUsername() {
        return getSharedPref().getString(USERNAME, "");
    }

    public void setUsername(String username) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(USERNAME, username);
        edit.apply();
    }

    public String getUserEmail() {
        return getSharedPref().getString(USER_EMAIL, "");
    }

    public void setUserEmail(String email) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(USER_EMAIL, email);
        edit.apply();
    }

    public String getUserMobile() {
        return getSharedPref().getString(USER_MOBILE, "");
    }

    public void setUserMobile(String mobile) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(USER_MOBILE, mobile);
        edit.apply();
    }

    public String getUserFullName() {
        return getSharedPref().getString(USER_FIRST_NAME, "");
    }

    public void setUserFullName(String fullName) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(USER_FIRST_NAME, fullName);
        edit.apply();
    }

    public String getUserShortBio() {
        return getSharedPref().getString(USER_SHORT_BIO, "");
    }

    public void setUserShortBio(String shortBio) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(USER_SHORT_BIO, shortBio);
        edit.apply();
    }

    public String getUserObject() {
        return getSharedPref().getString(USER_OBJECT, "");
    }

    public void setUserObject(String userObject) {
        SharedPreferences.Editor edit = getSharedPref().edit();
        edit.putString(USER_OBJECT, userObject);
        edit.apply();
    }

}
