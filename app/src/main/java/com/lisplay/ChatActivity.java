package com.lisplay;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class ChatActivity extends AppCompatActivity {
    private static final String TAG = "ChatActivity";
    private static final int SELECT_IMAGE = 15;

    @Inject
    PreferenceManager pm;

    private Activity activity;
    private boolean messageLoaded = false;
    private View club2camera;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private EditText chatMessage;
    private RecyclerView recyclerView;
    private ChatMessageAdapter chatAdapter;
    private List<ChatMessageModel> chatMessageList = new ArrayList<>();
    private CollectionReference club2ChatRef, club3ChatRef, club4ChatRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        activity = ChatActivity.this;
        ((MyApp) getApplication()).getNetComponent().inject(this);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        DocumentReference documentReference = db.collection("chat").document(pm.getUsername());
        club2ChatRef = documentReference.collection("club_2");
        club3ChatRef = documentReference.collection("club_3");
        club4ChatRef = documentReference.collection("club_4");

        chatAdapter = new ChatMessageAdapter(this, chatMessageList, pm.getUsername());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        recyclerView = findViewById(R.id.chat_club_2_rv);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(chatAdapter);

        club2camera = findViewById(R.id.club_2_camera);
        chatMessage = findViewById(R.id.chat_message);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new CustomPagerAdapter(this));
        viewPager.setOffscreenPageLimit(2);
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        initListeners();
        displayChatMessages();
    }

    private void initListeners() {
        findViewById(R.id.club_2_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(getPickImageChooserIntent(), SELECT_IMAGE);
            }
        });

        findViewById(R.id.send_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatMessage.getText().toString().trim().isEmpty()) return;
//                DocumentReference chat = club2ChatRef.document();
//                chat.getId();

                final ChatMessageModel newMessage = new ChatMessageModel(
                        chatMessage.getText().toString().trim(),
                        pm.getUserFullName(),
                        pm.getUsername(),
                        1);
                club2ChatRef.add(newMessage)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                chatAdapter.updateMessage(newMessage);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });
                chatMessage.setText("");
                addMessageToAdapter(newMessage);
            }
        });

        club2ChatRef.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots == null) return;
                if (!messageLoaded) return;
                Gson gson = new Gson();
                List<DocumentChange> changeList = queryDocumentSnapshots.getDocumentChanges();
                for (DocumentChange change : changeList) {
                    JsonElement jsonElement = gson.toJsonTree(change.getDocument().getData());
                    ChatMessageModel chatMessageModel = gson.fromJson(jsonElement, ChatMessageModel.class);
                    if (!chatMessageModel.getSenderUid().equals(pm.getUsername()))
                        addMessageToAdapter(chatMessageModel);
                }
            }
        });

        chatMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() == 0)
                    club2camera.setVisibility(View.VISIBLE);
                else
                    club2camera.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void addMessageToAdapter(ChatMessageModel chatMessageModel) {
        chatMessageList.add(chatMessageModel);
        chatAdapter.notifyItemInserted(chatAdapter.getItemCount());
        recyclerView.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
    }

    private void displayChatMessages() {
        club2ChatRef.orderBy("messageTime").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                messageLoaded = true;
                if (task.isSuccessful()) {
                    QuerySnapshot snapshot = task.getResult();
                    chatMessageList.addAll(snapshot.toObjects(ChatMessageModel.class));
                    chatAdapter.notifyItemInserted(chatAdapter.getItemCount());
//                    recyclerView.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });
    }

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        //  collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "image.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }

        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {

            try {
                Uri imageResultUri = getPickImageResultUri(data);
                final ChatMessageModel newMessage = new ChatMessageModel(
                        Utils.getRealPathFromUri(activity, imageResultUri),
                        chatMessage.getText().toString().trim(),
                        pm.getUserFullName(),
                        pm.getUsername(),
                        2);
                addMessageToAdapter(newMessage);

//                StorageReference riversRef = storageRef.child("images/" + imageResultUri.getLastPathSegment());
                final StorageReference riversRef = FirebaseStorage.getInstance().getReference().child(UUID.randomUUID().toString());
                String compressedImagePath = Utils.compressImage(activity, imageResultUri);
                UploadTask uploadTask = riversRef.putFile(Uri.fromFile(new File(compressedImagePath)));

                // Register observers to listen for when the download is done or if it fails
//                uploadTask.addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception exception) {
//                        // Handle unsuccessful uploads
//                    }
//                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
//                        taskSnapshot.getMetadata();
//                    }
//                });

                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return riversRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();
                            newMessage.setMediaUrl(downloadUri.toString());
                            club2ChatRef.add(newMessage)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                            chatAdapter.updateMessage(newMessage);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Error adding document", e);
                                        }
                                    });

                        } else {
                            Utils.showToast(activity, "Upload failed!");
                        }
                    }
                });

            } catch (Exception e) {
                Utils.showToast(this, "Failed! Please try again.");
            }

        }
    }

    private class CustomPagerAdapter extends PagerAdapter {

        public CustomPagerAdapter(Context context) {
        }

        @Override
        public CharSequence getPageTitle(int position) {
            super.getPageTitle(position);

            switch (position) {
                case 0:
                    return "CLUB 2";
                case 1:
                    return "CLUB 3";
                case 2:
                    return "CLUB 4";
                default:
                    return null;
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.chat_club_2;
                    tabLayout.setScrollPosition(0, 0f, true);
                    break;
                case 1:
                    resId = R.id.chat_club_3;
                    tabLayout.setScrollPosition(1, 0f, true);
                    break;
                case 2:
                    resId = R.id.chat_club_4;
                    tabLayout.setScrollPosition(2, 0f, true);
                    break;
            }
            return findViewById(resId);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }
}
