package com.lisplay;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.List;
import java.util.Locale;


public class SubscribersAdapter extends RecyclerView.Adapter<SubscribersAdapter.ItemViewHolder> {
    private List<DashboardModel.SubscribersActive> subscribers;
    private Activity activity;

    public SubscribersAdapter(Activity activity, List<DashboardModel.SubscribersActive> items) {
        this.activity = activity;
        this.subscribers = items;
    }

    @NonNull
    @Override
    public SubscribersAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_subscriber, parent, false);
        return new SubscribersAdapter.ItemViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubscribersAdapter.ItemViewHolder holder, final int position) {
        holder.fullName.setText(subscribers.get(position).getSubscriber().getFirstName());
        holder.username.setText(String.format("@%s", subscribers.get(position).getSubscriber().getUsername()));
        holder.subscriptionAmount.setText(String.format(Locale.getDefault(), "Rs. %d/month", subscribers.get(position).getAmount()));

        Glide.with(activity)
                .load(subscribers.get(position).getSubscriber().getPicture())
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.profile_image_placeholder)
                .error(R.drawable.profile_image_placeholder)
                .override(100, 100)
                .into(new BitmapImageViewTarget(holder.profilePicture) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(activity.getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.profilePicture.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e("ERROR", "Showing round profile picture failed", e);
                        }
                    }
                });

        holder.profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if (subscribers.get(holder.getAdapterPosition()).getSubscriber().getIsCreator())
                    intent = new Intent(activity, CreatorProfileActivity.class);
                else intent = new Intent(activity, UserProfileActivity.class);
                intent.putExtra("username", subscribers.get(holder.getAdapterPosition()).getSubscriber().getUsername());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subscribers.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        View profileLayout;
        ImageView profilePicture;
        TextView fullName, username, subscriptionAmount;

        public ItemViewHolder(View itemView) {
            super(itemView);
            profileLayout = itemView.findViewById(R.id.profile_layout);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            fullName = itemView.findViewById(R.id.first_name);
            username = itemView.findViewById(R.id.username);
            subscriptionAmount = itemView.findViewById(R.id.subscription_amount);
        }
    }
}

