package com.lisplay;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WelcomeActivity extends Activity {
    private static final String TAG = "WelcomeActivity";
    private static final int SELECT_PROFILE_IMAGE = 12;

    @Inject
    PreferenceManager pm;
    @Inject
    NetworkService networkService;

    private Activity activity;
    private EditText fullNameET;
    private View loadingView;
    private MultipartBody.Part body;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_welcome);
        activity = WelcomeActivity.this;

        loadingView = findViewById(R.id.loading_layout);
        fullNameET = findViewById(R.id.first_name);
        imageView = findViewById(R.id.profile_picture);
        findViewById(R.id.welcome_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullName = fullNameET.getText().toString().trim();
                if (fullName.isEmpty()) {
                    Utils.showToast(WelcomeActivity.this, "Please enter a name for this account.");
                    return;
                }
                updateUserName(fullName);
            }
        });
        findViewById(R.id.picture_intent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(getPickImageChooserIntent(), SELECT_PROFILE_IMAGE);
            }
        });

        if (pm.getUsername().isEmpty()) getLoggedUser();
        else getCompleteUserProfile(pm.getUsername());
    }

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

//         collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.jpeg"));
        }
        return outputFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {

            try {
                Uri imageResultUri = getPickImageResultUri(data);
                String tempImagePath = Utils.getRealPathFromUri(this, imageResultUri);
                final String imagePath = Utils.compressImage(tempImagePath);
                // create RequestBody instance from file
                File file = new File(imagePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
                // MultipartBody.Part is used to send also the actual file name
                body = MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

                Glide.with(this)
                        .load(imagePath)
                        .asBitmap()
                        .centerCrop()
                        .override(200, 200)
                        .into(new BitmapImageViewTarget(imageView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                try {
                                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                            .create(WelcomeActivity.this.getApplicationContext().getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    imageView.setImageDrawable(circularBitmapDrawable);

                                } catch (Exception e) {
                                    Log.e(TAG, "Showing round profile picture failed", e);
                                    imageView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                                }
                            }
                        });

            } catch (Exception e) {
                Utils.showToast(this, "Failed! Please try again.");
            }

        }
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }

        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void getLoggedUser() {
        Observable<ResponseModel> observable = networkService.getUser(pm.getToken());
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        loadingView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        pm.setUsername(model.getUsername());
                        pm.setUserEmail(model.getEmail());
                        pm.setUserFullName(model.getFirstName());
                        fullNameET.setText(model.getFirstName());

                        getCompleteUserProfile(pm.getUsername());
                    }
                });
    }

    private void getCompleteUserProfile(String username) {
        Observable<UserModel.User> observable = networkService.getUser(pm.getToken(), username);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        loadingView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        populateUserData(model);
                    }
                });
    }

    private void populateUserData(UserModel.User user) {
        fullNameET.setText(user.getFirstName());
        pm.setIsCreator(user.getIsCreator());
        pm.setUsername(user.getUsername());
        pm.setUserFullName(user.getFirstName());
        pm.setUserShortBio(user.getShortBio());
        pm.setUserEmail(user.getEmail());
        pm.setUserMobile(user.getMobile());
        pm.setUserObject(new Gson().toJson(user));

        Glide.with(this)
                .load(user.getPicture())
                .asBitmap()
                .centerCrop()
                .override(300, 300)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e(TAG, "Showing round profile picture failed", e);
                        }
                    }
                });

        if (FirebaseAuth.getInstance().getCurrentUser() == null)
            getFirebaseToken();
        else
            updateFirebaseUser();
    }


    private void getFirebaseToken() {
        Observable<ResponseModel> observable = networkService.getFirebaseToken(pm.getToken(), pm.getToken());
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        authFirebase(model.getResponseMessage());
                    }
                });
    }

    private void authFirebase(String customToken) {
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.signInWithCustomToken(customToken)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            updateFirebaseUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            Utils.showToast(activity, "Authentication failed! Please try again.");
                        }
                    }
                });
    }

    private void updateFirebaseUser() {
        loadingView.setVisibility(View.GONE);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) return;
        Type type = new TypeToken<UserModel.User>() {
        }.getType();
        UserModel.User savedUser = new Gson().fromJson(pm.getUserObject(), type);

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(savedUser.getFirstName())
                .setPhotoUri(Uri.parse(savedUser.getPicture()))
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Log.d(TAG, "User profile updated.");
//                        }
                    }
                });
        user.updateEmail(savedUser.getEmail())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Log.d(TAG, "User email address updated.");
//                        }
                    }
                });
    }

    private void updateUserName(String name) {
        if (!Utils.isInternetAvailable(this, true)) return;
        loadingView.setVisibility(View.VISIBLE);
        Map<String, RequestBody> map = new HashMap<>();
        map.put("first_name", RequestBody.create(okhttp3.MultipartBody.FORM, name));

        Observable<UserModel.User> observable = networkService.updateUser(pm.getToken(), map, body, null);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        loadingView.setVisibility(View.GONE);
                        Toast.makeText(WelcomeActivity.this, "Error! Please try again.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        pm.setUserFullName(model.getFirstName());
                        pm.setUserObject(new Gson().toJson(model));
                        pm.setAppStage(2);
                        Toast.makeText(WelcomeActivity.this, "PROFILE UPDATED!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(WelcomeActivity.this, MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                });

    }
}
