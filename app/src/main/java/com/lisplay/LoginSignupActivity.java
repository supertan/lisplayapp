package com.lisplay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONObject;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginSignupActivity extends Activity {
    private static final String TAG = "LoginSignupActivity";

    @Inject
    PreferenceManager pm;
    @Inject
    NetworkService networkService;

    private Activity activity;
    private View progressBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_login_signup);
        firebaseAuth = FirebaseAuth.getInstance();

        activity = LoginSignupActivity.this;
        progressBar = findViewById(R.id.progress_bar);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new CustomPagerAdapter(this));
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        if (BuildConfig.DEBUG) {
            ((EditText) findViewById(R.id.username_signup)).setText("app");
            ((EditText) findViewById(R.id.email_signup)).setText("app@mailinator.com");
            ((EditText) findViewById(R.id.password_signup)).setText("hello123");
            ((EditText) findViewById(R.id.password_confirm_signup)).setText("hello123");
            ((EditText) findViewById(R.id.password_login)).setText("hello123");
        }

        findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String username = ((EditText) findViewById(R.id.username_login)).getText().toString().trim();
                final String password = ((EditText) findViewById(R.id.password_login)).getText().toString().trim();
                if (!isLoginCredentialValid(username, password)) return;
                loginUser(username, password);
            }
        });

        findViewById(R.id.signup_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String username = ((EditText) findViewById(R.id.username_signup)).getText().toString().trim();
                final String email = ((EditText) findViewById(R.id.email_signup)).getText().toString().trim();
                final String password = ((EditText) findViewById(R.id.password_signup)).getText().toString().trim();
                final String passwordConfirm = ((EditText) findViewById(R.id.password_confirm_signup)).getText().toString().trim();
                if (!isSignupCredentialValid(username, email, password, passwordConfirm)) return;
                signupUser(username, email, password, passwordConfirm);
            }
        });
    }

    private boolean isLoginCredentialValid(String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            Utils.showToast(this, "Please enter username/email and password.");
            return false;
        }
        return true;
    }

    private boolean isSignupCredentialValid(String username, String email, String password, String passwordConfirm) {
        if (username.isEmpty() || email.isEmpty() || password.isEmpty() || passwordConfirm.isEmpty()) {
            Utils.showToast(this, "All fields are required to fill.");
            return false;
        }
        if (!email.contains("@") || !email.contains(".")) {
            Utils.showToast(this, "Please enter a valid email");
            return false;
        }
        if (!password.equals(passwordConfirm)) {
            Utils.showToast(this, "Password mismatch.");
            return false;
        }
        return true;
    }

    private void loginUser(final String username, String password) {
        if (!Utils.isInternetAvailable(this, true)) return;
        progressBar.setVisibility(View.VISIBLE);
        Observable<ResponseModel> observable = networkService.loginUser(username, password);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        progressBar.setVisibility(View.GONE);
                        try {
                            HttpException error = (HttpException) e;
                            String errorBody = error.response().errorBody().string();
                            JSONObject json = new JSONObject(errorBody);
                            if (json.has("non_field_errors") && json.getJSONArray("non_field_errors").length() > 0)
                                Toast.makeText(activity, "ERROR: " + json.getJSONArray("non_field_errors").get(0), Toast.LENGTH_LONG).show();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

//                        if (username.contains("@"))
//                            Utils.showToast(activity, "Wrong email or password.");
//                        else
//                            Utils.showToast(activity, "Wrong username or password.");
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        if (model.getDetail() != null && !model.getDetail().isEmpty()) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(activity, "ERROR: " + model.getDetail(), Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (model.getNonFieldErrors() != null && !model.getNonFieldErrors().isEmpty()) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(activity, "ERROR: " + model.getNonFieldErrors().get(0), Toast.LENGTH_LONG).show();
                            return;
                        }
                        pm.setToken("Token " + model.getKey());
                        pm.setUsername(model.getUsername());
                        pm.setUserEmail(model.getEmail());
                        pm.setAppStage(1);

                        getFirebaseToken();

//                        Intent intent;
//                        if (pm.getUserFullName().isEmpty())
//                            intent = new Intent(activity, WelcomeActivity.class);
//                        else
//                            intent = new Intent(activity, MainActivity.class);

//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        finish();
                    }
                });
    }

    private void getFirebaseToken() {
        Observable<ResponseModel> observable = networkService.getFirebaseToken(pm.getToken(), pm.getToken());
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        authFirebase(model.getResponseMessage());
                    }
                });
    }

    private void authFirebase(String customToken) {
        firebaseAuth.signInWithCustomToken(customToken)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();

                            Intent intent = new Intent(activity, WelcomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            Utils.showToast(activity, "Authentication failed! Please try again.");
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void signupUser(String username, String email, String password, String
            passwordAgain) {
        if (!Utils.isInternetAvailable(this, true)) return;
        progressBar.setVisibility(View.VISIBLE);
        Observable<ResponseModel> observable = networkService.signupUser(username, email, password, passwordAgain);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, e.getMessage());
                        try {
                            HttpException error = (HttpException) e;
                            String errorBody = error.response().errorBody().string();
                            JSONObject json = new JSONObject(errorBody);
                            if (json.has("username") && json.getJSONArray("username").length() > 0)
                                Toast.makeText(activity,
                                        "ERROR: " + json.getJSONArray("username").get(0),
                                        Toast.LENGTH_LONG).show();
                            if (json.has("email") && json.getJSONArray("email").length() > 0)
                                Toast.makeText(activity,
                                        "ERROR: " + json.getJSONArray("email").get(0),
                                        Toast.LENGTH_LONG).show();
                            if (json.has("password1") && json.getJSONArray("password1").length() > 0)
                                Toast.makeText(activity,
                                        "ERROR: " + json.getJSONArray("password1").get(0),
                                        Toast.LENGTH_LONG).show();
                            if (json.has("password2") && json.getJSONArray("password2").length() > 0)
                                Toast.makeText(activity,
                                        "ERROR: " + json.getJSONArray("password2").get(0),
                                        Toast.LENGTH_LONG).show();
                            if (json.has("non_field_errors") && json.getJSONArray("non_field_errors").length() > 0)
                                Toast.makeText(activity,
                                        "ERROR: " + json.getJSONArray("non_field_errors").get(0),
                                        Toast.LENGTH_LONG).show();

                        } catch (Exception e1) {
                            Toast.makeText(activity, "Error! Please try again.", Toast.LENGTH_LONG).show();
                            e1.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        progressBar.setVisibility(View.GONE);
                        if (model.getNonFieldErrors() != null && !model.getNonFieldErrors().isEmpty()) {
                            Toast.makeText(activity, "ERROR: " + model.getNonFieldErrors().get(0), Toast.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(activity, "SUCCESS: Registration email sent. Please check your email.", Toast.LENGTH_LONG).show();
                        finish();
                    }
                });

    }

    private class CustomPagerAdapter extends PagerAdapter {

        public CustomPagerAdapter(Context context) {
        }

        @Override
        public CharSequence getPageTitle(int position) {
            super.getPageTitle(position);

            switch (position) {
                case 0:
                    return "LOGIN";
                case 1:
                    return "SIGN UP";
                default:
                    return null;
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.view_1;
                    tabLayout.setScrollPosition(0, 0f, true);
                    break;
                case 1:
                    resId = R.id.view_2;
                    tabLayout.setScrollPosition(1, 0f, true);
                    break;
            }
            return findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}