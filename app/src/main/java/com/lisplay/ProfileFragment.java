package com.lisplay;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Objects;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "ProfileFragment";

    @Inject
    NetworkService networkService;
    @Inject
    PreferenceManager pm;

    private View root;
    private Activity activity;
    private ImageView featuredImage, profilePicture;
    private TextView fullName;
    private TextView shortBio;
    private TextView icFacebook, icTwitter, icInstagram, icYoutube, icGithub, icWebsite, icOther;

    public static ProfileFragment getInstance() {
        return new ProfileFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root != null) return root;
        root = inflater.inflate(R.layout.fragment_profile, container, false);
        activity = getActivity();
        ((MyApp) activity.getApplication()).getNetComponent().inject(this);

        Typeface fontAwesome = Typeface.createFromAsset(activity.getAssets(), "fonts/fa-brands-400.ttf");
        icFacebook = root.findViewById(R.id.icon_facebook);
        icTwitter = root.findViewById(R.id.icon_twitter);
        icInstagram = root.findViewById(R.id.icon_instagram);
        icYoutube = root.findViewById(R.id.icon_youtube);
        icGithub = root.findViewById(R.id.icon_github);
        icWebsite = root.findViewById(R.id.icon_website);
        icOther = root.findViewById(R.id.icon_other);
        icFacebook.setTypeface(fontAwesome);
        icTwitter.setTypeface(fontAwesome);
        icInstagram.setTypeface(fontAwesome);
        icYoutube.setTypeface(fontAwesome);
        icGithub.setTypeface(fontAwesome);
        icWebsite.setTypeface(fontAwesome);
        icOther.setTypeface(fontAwesome);

        ((TextView) root.findViewById(R.id.username)).setText(String.format("@%s", pm.getUsername()));
        fullName = root.findViewById(R.id.first_name);
        shortBio = root.findViewById(R.id.short_bio);
        featuredImage = root.findViewById(R.id.featured_image);
        profilePicture = root.findViewById(R.id.profile_picture);
        Type type = new TypeToken<UserModel.User>() {
        }.getType();
        UserModel.User savedUser = new Gson().fromJson(pm.getUserObject(), type);
        if (savedUser.getIsCreator())
            root.findViewById(R.id.become_creator_card).setVisibility(View.GONE);
        populateUserData(savedUser);

        root.findViewById(R.id.edit_profile).setOnClickListener(this);
        root.findViewById(R.id.become_creator).setOnClickListener(this);
        root.findViewById(R.id.clear_app_data).setOnClickListener(this);
        root.findViewById(R.id.explore_creators_button).setOnClickListener(this);
        root.findViewById(R.id.show_dashboard).setOnClickListener(this);
        root.findViewById(R.id.show_creator_details).setOnClickListener(this);
        root.findViewById(R.id.show_subscriptions).setOnClickListener(this);

        if (pm.getUsername().isEmpty()) getLoggedUser();
        else getUser(pm.getUsername());

        return root;
    }

    private void getLoggedUser() {
        Observable<ResponseModel> observable = networkService.getUser(pm.getToken());
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        pm.setUsername(model.getUsername());
                        pm.setUserEmail(model.getEmail());
                        getUser(model.getUsername());
                    }
                });
    }

    private void getUser(String username) {
        Observable<UserModel.User> observable = networkService.getUser(pm.getToken(), username);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        populateUserData(model);
                    }
                });
    }

    private void populateUserData(UserModel.User user) {
        fullName.setText(user.getFirstName());
        shortBio.setText(user.getShortBio());
        pm.setIsCreator(user.getIsCreator());
        pm.setUsername(user.getUsername());
        pm.setUserFullName(user.getFirstName());
        pm.setUserShortBio(user.getShortBio());
        pm.setUserEmail(user.getEmail());
        pm.setUserMobile(user.getMobile());
        pm.setUserObject(new Gson().toJson(user));

        Glide.with(this)
                .load(user.getFeaturedImage())
                .centerCrop()
                .into(featuredImage);

        Glide.with(this)
                .load(user.getPicture())
                .asBitmap()
                .centerCrop()
                .override(500, 500)
                .into(new BitmapImageViewTarget(profilePicture) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(activity.getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profilePicture.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e(TAG, "Showing round profile picture failed", e);
                        }
                    }
                });

        populateSocialLinks(user.getSocialLinks());
    }

    private void populateSocialLinks(String socialLinks) {
        try {
            JSONObject jsonObject = new JSONObject(socialLinks);

            final String facebookLink = jsonObject.get("facebook_link").toString();
            if (!facebookLink.isEmpty()) {
                icFacebook.setVisibility(View.VISIBLE);
                icFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookLink));
                        startActivity(intent);
                    }
                });
            }

            final String twitterLink = jsonObject.get("twitter_link").toString();
            if (!twitterLink.isEmpty()) {
                icTwitter.setVisibility(View.VISIBLE);
                icTwitter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterLink));
                        startActivity(intent);
                    }
                });
            }

            final String instagramLink = jsonObject.get("instagram_link").toString();
            if (!instagramLink.isEmpty()) {
                icInstagram.setVisibility(View.VISIBLE);
                icInstagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(instagramLink));
                        startActivity(intent);
                    }
                });
            }

            final String youtubeLink = jsonObject.get("youtube_link").toString();
            if (!youtubeLink.isEmpty()) {
                icYoutube.setVisibility(View.VISIBLE);
                icYoutube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink));
                        startActivity(intent);
                    }
                });
            }

            final String githubLink = jsonObject.get("github_link").toString();
            if (!githubLink.isEmpty()) {
                icGithub.setVisibility(View.VISIBLE);
                icGithub.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(githubLink));
                        startActivity(intent);
                    }
                });
            }

            final String websiteLink = jsonObject.get("website_link").toString();
            if (!websiteLink.isEmpty()) {
                icWebsite.setVisibility(View.VISIBLE);
                icWebsite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(websiteLink));
                        startActivity(intent);
                    }
                });
            }

            final String otherLink = jsonObject.get("other_link").toString();
            if (!otherLink.isEmpty()) {
                icOther.setVisibility(View.VISIBLE);
                icOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(otherLink));
                        startActivity(intent);
                    }
                });
            }

            Log.d(TAG, jsonObject.get("website_link").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_profile:
                activity.startActivity(new Intent(activity, EditProfileActivity.class));
                break;

            case R.id.become_creator:
            case R.id.show_creator_details:
                activity.startActivity(new Intent(activity, CreatorDetailActivity.class));
                break;

            case R.id.clear_app_data:
                ((ActivityManager) Objects.requireNonNull(activity.getSystemService(Context.ACTIVITY_SERVICE))).clearApplicationUserData();
                break;

            case R.id.explore_creators_button:
                ((MainActivity) activity).setTab(0);
                break;

            case R.id.show_dashboard:
                if (!pm.isCreator()) {
                    Utils.showToast(activity, "Dashboard is available only for creators.");
                    break;
                }
                startActivity(new Intent(activity, DashboardActivity.class));
                break;

            case R.id.show_subscriptions:
                startActivity(new Intent(activity, SubscriptionsActivity.class));
                break;
        }
    }
}
