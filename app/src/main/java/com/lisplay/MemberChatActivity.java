package com.lisplay;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class MemberChatActivity extends AppCompatActivity {
    private static final String TAG = "MemberChatActivity";

    @Inject
    PreferenceManager pm;

    private Activity activity;
    private boolean messageLoaded = false;
    private EditText chatMessage;
    private String username, club;
    private CollectionReference clubChatRef;
    private ChatMessageAdapter chatAdapter;
    private List<ChatMessageModel> chatMessageList = new ArrayList<>();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_chat);
        activity = MemberChatActivity.this;
        ((MyApp) getApplication()).getNetComponent().inject(this);

        username = getIntent().getStringExtra("username");
        club = getIntent().getStringExtra("club");

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        DocumentReference documentReference = db.collection("chat").document(username);
        clubChatRef = documentReference.collection(club);

        chatMessage = findViewById(R.id.chat_message);
        chatAdapter = new ChatMessageAdapter(this, chatMessageList, pm.getUsername());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        recyclerView = findViewById(R.id.chat_rv);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(chatAdapter);

        initListeners();
        displayChatMessages();
    }


    private void initListeners() {
        findViewById(R.id.send_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatMessage.getText().toString().trim().isEmpty()) return;
//                DocumentReference chat = club2ChatRef.document();
//                chat.getId();

                final ChatMessageModel newMessage = new ChatMessageModel(
                        chatMessage.getText().toString().trim(),
                        pm.getUserFullName(),
                        pm.getUsername(),
                        1);
                clubChatRef.add(newMessage)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                chatAdapter.updateMessage(newMessage);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });

                chatMessage.setText("");
                addMessageToAdapter(newMessage);
            }
        });

        clubChatRef.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots == null) return;
                if (!messageLoaded) return;
                Gson gson = new Gson();
                List<DocumentChange> changeList = queryDocumentSnapshots.getDocumentChanges();
                for (DocumentChange change : changeList) {
                    JsonElement jsonElement = gson.toJsonTree(change.getDocument().getData());
                    ChatMessageModel chatMessageModel = gson.fromJson(jsonElement, ChatMessageModel.class);
                    if (!chatMessageModel.getSenderUid().equals(pm.getUsername()))
                        addMessageToAdapter(chatMessageModel);
                }
            }
        });
    }

    private void addMessageToAdapter(ChatMessageModel chatMessageModel) {
        chatMessageList.add(chatMessageModel);
        chatAdapter.notifyItemInserted(chatAdapter.getItemCount());
        recyclerView.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
    }

    private void displayChatMessages() {
        clubChatRef.orderBy("messageTime").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                messageLoaded = true;
                if (task.isSuccessful()) {
                    QuerySnapshot snapshot = task.getResult();
                    chatMessageList.addAll(snapshot.toObjects(ChatMessageModel.class));
                    chatAdapter.notifyItemInserted(chatAdapter.getItemCount());
//                    recyclerView.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });
    }
}
