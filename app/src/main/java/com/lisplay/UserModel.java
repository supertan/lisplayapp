package com.lisplay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModel {

    @SerializedName("users")
    @Expose
    private List<User> users = null;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public class User {

        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("short_bio")
        @Expose
        private String shortBio;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("profile_description")
        @Expose
        private String profileDescription;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("featured_image")
        @Expose
        private String featuredImage;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("club_2_reward")
        @Expose
        private String club2Reward;
        @SerializedName("club_3_reward")
        @Expose
        private String club3Reward;
        @SerializedName("club_4_reward")
        @Expose
        private String club4Reward;
        @SerializedName("social_links")
        @Expose
        private String socialLinks;
        @SerializedName("featured_video")
        @Expose
        private String featuredVideo;
        @SerializedName("featured_text") // Featured text is Thank you note.
        @Expose
        private String featuredText;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("is_creator")
        @Expose
        private Boolean isCreator;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getShortBio() {
            return shortBio;
        }

        public void setShortBio(String shortBio) {
            this.shortBio = shortBio;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfileDescription() {
            return profileDescription;
        }

        public void setProfileDescription(String profileDescription) {
            this.profileDescription = profileDescription;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getFeaturedImage() {
            return featuredImage;
        }

        public void setFeaturedImage(String featuredImage) {
            this.featuredImage = featuredImage;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getClub2Reward() {
            return club2Reward;
        }

        public void setClub2Reward(String club2Reward) {
            this.club2Reward = club2Reward;
        }

        public String getClub3Reward() {
            return club3Reward;
        }

        public void setClub3Reward(String club3Reward) {
            this.club3Reward = club3Reward;
        }

        public String getClub4Reward() {
            return club4Reward;
        }

        public void setClub4Reward(String club4Reward) {
            this.club4Reward = club4Reward;
        }

        public String getSocialLinks() {
            return socialLinks;
        }

        public void setSocialLinks(String socialLinks) {
            this.socialLinks = socialLinks;
        }

        public String getFeaturedVideo() {
            return featuredVideo;
        }

        public void setFeaturedVideo(String featuredVideo) {
            this.featuredVideo = featuredVideo;
        }

        public String getFeaturedText() { // Featured text is Thank you note.
            return featuredText;
        }

        public void setFeaturedText(String featuredText) {
            this.featuredText = featuredText;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Boolean getIsCreator() {
            return isCreator;
        }

        public void setIsCreator(Boolean isCreator) {
            this.isCreator = isCreator;
        }

    }

}