package com.lisplay;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class CreatorDetailActivity extends Activity {
    private static final String TAG = "CreatorDetailActivity";

    @Inject
    PreferenceManager pm;
    @Inject
    NetworkService networkService;

    private Switch isCreator;
    private Spinner creatorCategory;
    private TextView creatorCategoryError, isCreatorError;
    private EditText fullName, shortBio, profileDescription, thankYouNote, featuredVideo, club2Reward, club3Reward, club4Reward;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String pocket_money_options[] = {
            "Select category",
            "Music",
            "Media",
            "Games",
            "Comedy",
            "Writing",
            "Podcasts",
            "Education",
            "Photography",
            "Programming",
            "Crafts and DIY",
            "Videos and Films",
            "Dance and Theater",
            "Drawing and Painting",
            "Science and Technology",
            "Others"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_creator_detail);

        isCreator = findViewById(R.id.is_creator);
        creatorCategory = findViewById(R.id.creator_category);
        creatorCategoryError = findViewById(R.id.creator_category_error);
        isCreatorError = findViewById(R.id.is_creator_error);
        fullName = findViewById(R.id.first_name);
        shortBio = findViewById(R.id.short_bio);
        profileDescription = findViewById(R.id.profile_description);
        thankYouNote = findViewById(R.id.thank_you_note);
        featuredVideo = findViewById(R.id.featured_video);
        club2Reward = findViewById(R.id.club_2_reward);
        club3Reward = findViewById(R.id.club_3_reward);
        club4Reward = findViewById(R.id.club_4_reward);

        Type type = new TypeToken<UserModel.User>() {
        }.getType();
        UserModel.User savedUser = new Gson().fromJson(pm.getUserObject(), type);
        populateDetails(savedUser);

        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new CustomPagerAdapter(this));
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ArrayAdapter mSpinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, pocket_money_options);
        mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        creatorCategory.setAdapter(mSpinnerAdapter);
        creatorCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                creatorCategoryError.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
        int spinnerPosition = mSpinnerAdapter.getPosition(savedUser.getCategory());
        creatorCategory.setSelection(spinnerPosition);
        isCreator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCreatorError.setText("");
            }
        });

        findViewById(R.id.update_creator_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasErrors()) return;
                updateCreatorDetails();
            }
        });
        findViewById(R.id.update_creator_rewards).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCreatorRewards();
            }
        });
    }

    private void populateDetails(UserModel.User savedUser) {
        isCreator.setChecked(savedUser.getIsCreator());
        fullName.setText(savedUser.getFirstName());
        shortBio.setText(savedUser.getShortBio());
        profileDescription.setText(savedUser.getProfileDescription());
        thankYouNote.setText(savedUser.getFeaturedText()); // Featured text is Thank you note.
        featuredVideo.setText(savedUser.getFeaturedVideo());

        club2Reward.setText(savedUser.getClub2Reward());
        club3Reward.setText(savedUser.getClub3Reward());
        club4Reward.setText(savedUser.getClub4Reward());
    }

    private boolean hasErrors() {
        boolean hasError = false;

        if (!isCreator.isChecked()) {
            isCreatorError.setText(R.string.please_turn_this_on);
            hasError = true;
        } else {
            isCreatorError.setText("");
        }
        if (creatorCategory.getSelectedItem().toString().equalsIgnoreCase("Select category")) {
            creatorCategoryError.setText(R.string.please_select_a_category);
            hasError = true;
        } else {
            creatorCategoryError.setText("");
        }

        if (fullName.getText().toString().isEmpty() || fullName.getText().length() > 30) {
            fullName.setError("Please enter a valid name");
            hasError = true;
        }

        if (shortBio.getText().length() < 10 || shortBio.getText().length() > 50) {
            shortBio.setError("Please enter a valid short bio");
            hasError = true;
        }

        return hasError;
    }

    private void updateCreatorDetails() {
        Map<String, String> map = new HashMap<>();
        map.put("is_creator", String.valueOf(isCreator.isChecked()));
        map.put("category", creatorCategory.getSelectedItem().toString());
        map.put("first_name", fullName.getText().toString());
        map.put("short_bio", shortBio.getText().toString());
        map.put("profile_description", profileDescription.getText().toString());
        map.put("featured_text", thankYouNote.getText().toString());
        map.put("featured_video", featuredVideo.getText().toString());

        Observable<UserModel.User> observable = networkService.updateCreator(pm.getToken(), map);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        Toast.makeText(CreatorDetailActivity.this, "Error! Please try again.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        if (model.getIsCreator()) {
                            pm.setUserFullName(model.getFirstName());
                            pm.setUserShortBio(model.getShortBio());
                            pm.setUserMobile(model.getMobile());
                            pm.setIsCreator(model.getIsCreator());
                            pm.setUserObject(new Gson().toJson(model));
                            Toast.makeText(CreatorDetailActivity.this, "CREATOR UPDATED!", Toast.LENGTH_LONG).show();
                            finish();
                        } else
                            Utils.showToast(CreatorDetailActivity.this, "FAILED TO UPDATE");
                    }
                });

    }

    private void updateCreatorRewards() {
        if (!pm.isCreator()) {
            Utils.showToast(this, "Only creators can update rewards.");
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("is_creator", String.valueOf(isCreator.isChecked()));
        map.put("club_2_reward", club2Reward.getText().toString());
        map.put("club_3_reward", club3Reward.getText().toString());
        map.put("club_4_reward", club4Reward.getText().toString());

        Observable<UserModel.User> observable = networkService.updateCreator(pm.getToken(), map);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        Toast.makeText(CreatorDetailActivity.this, "Error! Please try again.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        if (model.getIsCreator()) {
                            pm.setUserFullName(model.getFirstName());
                            pm.setUserShortBio(model.getShortBio());
                            pm.setUserMobile(model.getMobile());
                            pm.setIsCreator(model.getIsCreator());
                            pm.setUserObject(new Gson().toJson(model));
                            Toast.makeText(CreatorDetailActivity.this, "REWARDS UPDATED!", Toast.LENGTH_LONG).show();
                            finish();
                        } else
                            Utils.showToast(CreatorDetailActivity.this, "FAILED TO UPDATE");
                    }
                });

    }

    private class CustomPagerAdapter extends PagerAdapter {

        public CustomPagerAdapter(Context context) {
        }

        @Override
        public CharSequence getPageTitle(int position) {
            super.getPageTitle(position);

            switch (position) {
                case 0:
                    return "DETAILS";
                case 1:
                    return "REWARDS";
                default:
                    return null;
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.view_creator_details;
                    tabLayout.setScrollPosition(0, 0f, true);
                    break;
                case 1:
                    resId = R.id.view_creator_rewards;
                    tabLayout.setScrollPosition(1, 0f, true);
                    break;
            }
            return findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }

}
