package com.lisplay;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EditProfileActivity extends Activity {
    private static final String TAG = "EditProfileActivity";
    private static final int SELECT_PROFILE_IMAGE = 21;
    private static final int SELECT_FEATURED_IMAGE = 22;

    @Inject
    PreferenceManager pm;
    @Inject
    NetworkService networkService;

    private MultipartBody.Part profilePictureBody, featuredImageBody;
    private EditText fullName, shortBio, mobile;
    private ImageView profileImage, featuredImage;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_edit_profile);

        fullName = findViewById(R.id.first_name);
        shortBio = findViewById(R.id.short_bio);
        mobile = findViewById(R.id.mobile);
        fullName.setText(pm.getUserFullName());
        shortBio.setText(pm.getUserShortBio());
        mobile.setText(pm.getUserMobile());
        profileImage = findViewById(R.id.profile_image);
        featuredImage = findViewById(R.id.featured_image);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new CustomPagerAdapter(this));
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Type type = new TypeToken<UserModel.User>() {
        }.getType();
        UserModel.User savedUser = new Gson().fromJson(pm.getUserObject(), type);
        populateImages(savedUser);
        populateSocialDetails(savedUser);

        findViewById(R.id.select_profile_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(getPickImageChooserIntent(), SELECT_PROFILE_IMAGE);
            }
        });
        findViewById(R.id.select_featured_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(getPickImageChooserIntent(), SELECT_FEATURED_IMAGE);
            }
        });
        findViewById(R.id.update_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser(fullName.getText().toString(),
                        shortBio.getText().toString(),
                        mobile.getText().toString());
            }
        });
        findViewById(R.id.update_social_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSocialDetails();
            }
        });
    }

    private void populateImages(final UserModel.User user) {
        Glide.with(this)
                .load(user.getPicture())
                .asBitmap()
                .centerCrop()
                .override(200, 200)
                .into(new BitmapImageViewTarget(profileImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(EditProfileActivity.this.getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profileImage.setImageDrawable(circularBitmapDrawable);

                        } catch (Exception e) {
                            Log.e(TAG, "Showing round profile picture failed", e);
                            profileImage.setImageBitmap(BitmapFactory.decodeFile(user.getPicture()));
                        }
                    }
                });

        Glide.with(this)
                .load(user.getFeaturedImage())
                .override(240, 140)
                .centerCrop()
                .into(featuredImage);
    }

    private void populateSocialDetails(final UserModel.User user) {
        if (user.getSocialLinks().isEmpty()) return;
        Type type = new TypeToken<HashMap>() {
        }.getType();
        HashMap<String, String> socialMap = new Gson().fromJson(user.getSocialLinks(), type);

        ((EditText) findViewById(R.id.facebook)).setText(socialMap.get("facebook_link"));
        ((EditText) findViewById(R.id.twitter)).setText(socialMap.get("twitter_link"));
        ((EditText) findViewById(R.id.instagram)).setText(socialMap.get("instagram_link"));
        ((EditText) findViewById(R.id.youtube)).setText(socialMap.get("youtube_link"));
        ((EditText) findViewById(R.id.soundcloud)).setText(socialMap.get("soundcloud_link"));
        ((EditText) findViewById(R.id.linkedin)).setText(socialMap.get("linkedin_link"));
        ((EditText) findViewById(R.id.blog)).setText(socialMap.get("blog_link"));
        ((EditText) findViewById(R.id.website)).setText(socialMap.get("website_link"));
        ((EditText) findViewById(R.id.play_store)).setText(socialMap.get("playstore_link"));
        ((EditText) findViewById(R.id.app_store)).setText(socialMap.get("appstore_link"));
        ((EditText) findViewById(R.id.github)).setText(socialMap.get("github_link"));
        ((EditText) findViewById(R.id.other)).setText(socialMap.get("other_link"));
    }

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

//         collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.jpeg"));
        }
        return outputFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == SELECT_PROFILE_IMAGE) {
            try {
                Uri imageResultUri = getPickImageResultUri(data);
                String tempImagePath = Utils.getRealPathFromUri(this, imageResultUri);
                final String imagePath = Utils.compressImage(tempImagePath);
                // create RequestBody instance from file
                File file = new File(imagePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
                // MultipartBody.Part is used to send also the actual file name
                profilePictureBody = MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

                Glide.with(this)
                        .load(imagePath)
                        .asBitmap()
                        .centerCrop()
                        .override(200, 200)
                        .into(new BitmapImageViewTarget(profileImage) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                try {
                                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                            .create(EditProfileActivity.this.getApplicationContext().getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    profileImage.setImageDrawable(circularBitmapDrawable);

                                } catch (Exception e) {
                                    Log.e(TAG, "Showing round profile picture failed", e);
                                    profileImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                                }
                            }
                        });

            } catch (Exception e) {
                Utils.showToast(this, "Failed! Please try again.");
            }

        } else if (requestCode == SELECT_FEATURED_IMAGE) {
            try {
                Uri imageResultUri = getPickImageResultUri(data);
                String tempImagePath = Utils.getRealPathFromUri(this, imageResultUri);
                final String imagePath = Utils.compressImage(tempImagePath);
                // create RequestBody instance from file
                File file = new File(imagePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
                // MultipartBody.Part is used to send also the actual file name
                featuredImageBody = MultipartBody.Part.createFormData("featured_image", file.getName(), requestFile);
                Glide.with(this)
                        .load(imagePath)
                        .override(240, 140)
                        .centerCrop()
                        .into(featuredImage);


            } catch (Exception e) {
                Utils.showToast(this, "Failed! Please try again.");
            }

        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }

        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void updateUser(String name, String shortBio, String mobile) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("first_name", RequestBody.create(okhttp3.MultipartBody.FORM, name));
        map.put("short_bio", RequestBody.create(okhttp3.MultipartBody.FORM, shortBio));
        map.put("mobile", RequestBody.create(okhttp3.MultipartBody.FORM, mobile));

        Observable<UserModel.User> observable = networkService.updateUser(pm.getToken(), map, profilePictureBody, featuredImageBody);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        Toast.makeText(EditProfileActivity.this, "Error! Please try again.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        pm.setUserFullName(model.getFirstName());
                        pm.setUserShortBio(model.getShortBio());
                        pm.setUserMobile(model.getMobile());
                        pm.setUserObject(new Gson().toJson(model));
                        Toast.makeText(EditProfileActivity.this, "PROFILE UPDATED!", Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void updateSocialDetails() {
        Map<String, String> map = new HashMap<>();
        map.put("facebook_link", ((EditText) findViewById(R.id.facebook)).getText().toString().trim());
        map.put("twitter_link", ((EditText) findViewById(R.id.twitter)).getText().toString().trim());
        map.put("instagram_link", ((EditText) findViewById(R.id.instagram)).getText().toString().trim());
        map.put("youtube_link", ((EditText) findViewById(R.id.youtube)).getText().toString().trim());
        map.put("soundcloud_link", ((EditText) findViewById(R.id.soundcloud)).getText().toString().trim());
        map.put("linkedin_link", ((EditText) findViewById(R.id.linkedin)).getText().toString().trim());
        map.put("blog_link", ((EditText) findViewById(R.id.blog)).getText().toString().trim());
        map.put("website_link", ((EditText) findViewById(R.id.website)).getText().toString().trim());
        map.put("playstore_link", ((EditText) findViewById(R.id.play_store)).getText().toString().trim());
        map.put("appstore_link", ((EditText) findViewById(R.id.app_store)).getText().toString().trim());
        map.put("github_link", ((EditText) findViewById(R.id.github)).getText().toString().trim());
        map.put("other_link", ((EditText) findViewById(R.id.other)).getText().toString().trim());

        Observable<UserModel.User> observable = networkService.updateSocialDetails(pm.getToken(), map);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        Toast.makeText(EditProfileActivity.this, "Error! Please try again.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        pm.setUserFullName(model.getFirstName());
                        pm.setUserShortBio(model.getShortBio());
                        pm.setUserMobile(model.getMobile());
                        pm.setUserObject(new Gson().toJson(model));
                        Toast.makeText(EditProfileActivity.this, "SOCIAL DETAILS UPDATED", Toast.LENGTH_LONG).show();
                    }
                });

    }

    private class CustomPagerAdapter extends PagerAdapter {

        public CustomPagerAdapter(Context context) {
        }

        @Override
        public CharSequence getPageTitle(int position) {
            super.getPageTitle(position);

            switch (position) {
                case 0:
                    return "PROFILE";
                case 1:
                    return "SOCIAL";
                default:
                    return null;
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.view_profile_edit;
                    tabLayout.setScrollPosition(0, 0f, true);
                    break;
                case 1:
                    resId = R.id.view_social_edit;
                    tabLayout.setScrollPosition(1, 0f, true);
                    break;
            }
            return findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
