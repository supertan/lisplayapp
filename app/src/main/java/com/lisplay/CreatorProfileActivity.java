package com.lisplay;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class CreatorProfileActivity extends AppCompatActivity implements View.OnClickListener, AppBarLayout.OnOffsetChangedListener {
    private static final String TAG = "CreatorProfileActivity";
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;

    private UserModel.User creator;
    private ImageView featuredImage, profilePicture;
    private TextView fullName, shortBio, profileDescription;
    private TextView clubTitle, clubText, club2Select, club3Select, club4Select;
    private ImageView clubHeaderCardBackground;
    private EditText amountEdittext;
    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer youTubePlayer = null;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private TextView icFacebook, icTwitter, icInstagram, icYoutube, icGithub, icWebsite, icOther;
    private int mMaxScrollSize;
    private boolean mIsAvatarShown = true;

    @Inject
    NetworkService networkService;
    @Inject
    PreferenceManager pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_creator);
        Typeface fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fa-brands-400.ttf");
        icFacebook = findViewById(R.id.icon_facebook);
        icTwitter = findViewById(R.id.icon_twitter);
        icInstagram = findViewById(R.id.icon_instagram);
        icYoutube = findViewById(R.id.icon_youtube);
        icGithub = findViewById(R.id.icon_github);
        icWebsite = findViewById(R.id.icon_website);
        icOther = findViewById(R.id.icon_other);

        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new CustomPagerAdapter(this));
        tabLayout = findViewById(R.id.htab_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null)
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout = findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appBarLayout.getTotalScrollRange();

        fullName = findViewById(R.id.first_name);
        shortBio = findViewById(R.id.short_bio);
        profileDescription = findViewById(R.id.profile_description);
        featuredImage = findViewById(R.id.featured_image);
        profilePicture = findViewById(R.id.profile_picture);
        amountEdittext = findViewById(R.id.amount);
        youTubePlayerView = findViewById(R.id.youtube_player_view);
        clubText = findViewById(R.id.club_text);
        clubTitle = findViewById(R.id.club_title);
        clubHeaderCardBackground = findViewById(R.id.club_header_card_background);
        club2Select = findViewById(R.id.club_2_select);
        club3Select = findViewById(R.id.club_3_select);
        club4Select = findViewById(R.id.club_4_select);

        findViewById(R.id.create_subscription).setOnClickListener(this);
        club2Select.setOnClickListener(this);
        club3Select.setOnClickListener(this);
        club4Select.setOnClickListener(this);
        icFacebook.setTypeface(fontAwesome);
        icTwitter.setTypeface(fontAwesome);
        icInstagram.setTypeface(fontAwesome);
        icYoutube.setTypeface(fontAwesome);
        icGithub.setTypeface(fontAwesome);
        icWebsite.setTypeface(fontAwesome);
        icOther.setTypeface(fontAwesome);

        String username = getIntent().getStringExtra("username");
        ((TextView) findViewById(R.id.username)).setText(String.format("@%s", username));
        getUser(username);
    }

    private void getUser(String username) {
        Observable<UserModel.User> observable = networkService.getUser(pm.getToken(), username);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        creator = model;
                        populateUserData(model);
                    }
                });
    }

    private void populateUserData(UserModel.User user) {
        fullName.setText(user.getFirstName());
        shortBio.setText(user.getShortBio());
        profileDescription.setText(user.getProfileDescription());
        loadVideo(user.getFeaturedVideo());
        populateSocialLinks(user.getSocialLinks());

        Glide.with(this)
                .load(user.getFeaturedImage())
                .centerCrop()
                .into(featuredImage);

        Glide.with(this)
                .load(user.getPicture())
                .asBitmap()
                .centerCrop()
                .override(500, 500)
                .into(new BitmapImageViewTarget(profilePicture) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(CreatorProfileActivity.this.getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profilePicture.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e(TAG, "Showing round profile picture failed", e);
                        }
                    }
                });
    }

    private void loadVideo(String url) {
        final String videoId = url.substring(url.length() - 11, url.length());
        Log.d(TAG, "videoId: " + videoId);

        youTubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        youTubePlayer = initializedYouTubePlayer;
                        initializedYouTubePlayer.cueVideo(videoId, 0);
                    }
                });
            }
        }, true);

    }

    private void populateSocialLinks(String socialLinks) {
        if (socialLinks == null || socialLinks.isEmpty()) {
            findViewById(R.id.social_media).setVisibility(View.GONE);
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(socialLinks);

            final String facebookLink = jsonObject.get("facebook_link").toString();
            if (!facebookLink.isEmpty()) {
                icFacebook.setVisibility(View.VISIBLE);
                icFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookLink));
                        startActivity(intent);
                    }
                });
            }

            final String twitterLink = jsonObject.get("twitter_link").toString();
            if (!twitterLink.isEmpty()) {
                icTwitter.setVisibility(View.VISIBLE);
                icTwitter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterLink));
                        startActivity(intent);
                    }
                });
            }

            final String instagramLink = jsonObject.get("instagram_link").toString();
            if (!instagramLink.isEmpty()) {
                icInstagram.setVisibility(View.VISIBLE);
                icInstagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(instagramLink));
                        startActivity(intent);
                    }
                });
            }

            final String youtubeLink = jsonObject.get("youtube_link").toString();
            if (!youtubeLink.isEmpty()) {
                icYoutube.setVisibility(View.VISIBLE);
                icYoutube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink));
                        startActivity(intent);
                    }
                });
            }

            final String githubLink = jsonObject.get("github_link").toString();
            if (!githubLink.isEmpty()) {
                icGithub.setVisibility(View.VISIBLE);
                icGithub.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(githubLink));
                        startActivity(intent);
                    }
                });
            }

            final String websiteLink = jsonObject.get("website_link").toString();
            if (!websiteLink.isEmpty()) {
                icWebsite.setVisibility(View.VISIBLE);
                icWebsite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(websiteLink));
                        startActivity(intent);
                    }
                });
            }

            final String otherLink = jsonObject.get("other_link").toString();
            if (!otherLink.isEmpty()) {
                icOther.setVisibility(View.VISIBLE);
                icOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(otherLink));
                        startActivity(intent);
                    }
                });
            }

            Log.d(TAG, jsonObject.get("website_link").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean isAmountValid() {
        String amountStr = amountEdittext.getText().toString();
        if (amountStr.isEmpty()) {
            Utils.showToast(this, "Please enter a valid amount.");
            return false;
        }
        int amount = Integer.parseInt(amountStr);
        if (amount < 10) {
            Utils.showToast(this, "Please enter an amount greater than or equal to 10.");
            return false;
        }
        if (amount > 10000) {
            Utils.showToast(this, "Please enter an amount less than 10000.");
            return false;
        }

        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            youTubePlayer.pause();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        youTubePlayerView.release();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_subscription:
                Intent intent = new Intent(CreatorProfileActivity.this, SubscriptionPaymentActivity.class);
                if (!isAmountValid()) break;
                intent.putExtra("amount", amountEdittext.getText().toString());
                intent.putExtra("creator_username", creator.getUsername());
                intent.putExtra("creator_first_name", creator.getFirstName());
                startActivity(intent);
                break;

            case R.id.club_2_select:
                clubText.setText(R.string.club_2_text);
                clubTitle.setText(R.string.club_2);
                amountEdittext.setHint("Your favourite 2 digit number");
                clubHeaderCardBackground.setImageResource(R.drawable.gradient_green_primary);
                club2Select.setBackgroundResource(R.drawable.rounded_rectangle_purple_light);
                club3Select.setBackgroundResource(0);
                club4Select.setBackgroundResource(0);
                break;

            case R.id.club_3_select:
                clubText.setText(R.string.club_3_text);
                clubTitle.setText(R.string.club_3);
                amountEdittext.setHint("Your favourite 3 digit number");
                clubHeaderCardBackground.setImageResource(R.drawable.gradient_red_primary);
                club2Select.setBackgroundResource(0);
                club3Select.setBackgroundResource(R.drawable.rounded_rectangle_purple_light);
                club4Select.setBackgroundResource(0);
                break;

            case R.id.club_4_select:
                clubText.setText(R.string.club_4_text);
                clubTitle.setText(R.string.club_4);
                amountEdittext.setHint("Your favourite 4 digit number");
                clubHeaderCardBackground.setImageResource(R.drawable.gradient_blue_primary);
                club2Select.setBackgroundResource(0);
                club3Select.setBackgroundResource(0);
                club4Select.setBackgroundResource(R.drawable.rounded_rectangle_purple_light);
                break;
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(verticalOffset)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            profilePicture.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            profilePicture.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }

    }

    private class CustomPagerAdapter extends PagerAdapter {


        public CustomPagerAdapter(Context context) {
        }

        @Override
        public CharSequence getPageTitle(int position) {
            super.getPageTitle(position);

            switch (position) {
                case 0:
                    return "ABOUT";
                case 1:
                    return "MEMBERSHIP";
                default:
                    return null;
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.view_1;
                    tabLayout.setScrollPosition(0, 0f, true);
                    break;
                case 1:
                    resId = R.id.view_2;
                    tabLayout.setScrollPosition(1, 0f, true);
                    break;
            }
            return findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
