package com.lisplay;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionModel {

    @SerializedName("subscriptions")
    @Expose
    private List<Subscription> subscriptions = null;

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public class Subscription {

        @SerializedName("subscription_id")
        @Expose
        private String subscriptionId;
        @SerializedName("plan")
        @Expose
        private Integer plan;
        @SerializedName("subscriber")
        @Expose
        private UserModel.User subscriber;
        @SerializedName("creator")
        @Expose
        private UserModel.User creator;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("paid_count")
        @Expose
        private Integer paidCount;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public String getSubscriptionId() {
            return subscriptionId;
        }

        public void setSubscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
        }

        public Integer getPlan() {
            return plan;
        }

        public void setPlan(Integer plan) {
            this.plan = plan;
        }

        public UserModel.User getSubscriber() {
            return subscriber;
        }

        public void setSubscriber(UserModel.User subscriber) {
            this.subscriber = subscriber;
        }

        public UserModel.User getCreator() {
            return creator;
        }

        public void setCreator(UserModel.User creator) {
            this.creator = creator;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public Integer getPaidCount() {
            return paidCount;
        }

        public void setPaidCount(Integer paidCount) {
            this.paidCount = paidCount;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }

}