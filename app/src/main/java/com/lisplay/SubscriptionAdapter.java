package com.lisplay;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.List;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ItemViewHolder> {
    private List<DashboardModel.SubscribersActive> subscriptions;
    private Activity activity;

    public SubscriptionAdapter(Activity activity, List<DashboardModel.SubscribersActive> items) {
        this.activity = activity;
        this.subscriptions = items;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_subscription, parent, false);
        return new ItemViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, int position) {
        holder.fullName.setText(subscriptions.get(position).getCreator().getFirstName());
        holder.username.setText(String.format("@%s", subscriptions.get(position).getCreator().getUsername()));
        int subscriptionAmount = subscriptions.get(position).getAmount();
        final String club;

        if (subscriptionAmount < 100) {
            club = "club_2";
            holder.membershipType.setText(activity.getString(R.string.club_2_membership));
        } else if (subscriptionAmount < 1000) {
            club = "club_3";
            holder.membershipType.setText(activity.getString(R.string.club_3_membership));
        } else {
            club = "club_4";
            holder.membershipType.setText(activity.getString(R.string.club_4_membership));
        }

        Glide.with(activity)
                .load(subscriptions.get(position).getCreator().getPicture())
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.profile_image_placeholder)
                .error(R.drawable.profile_image_placeholder)
                .override(100, 100)
                .into(new BitmapImageViewTarget(holder.profilePicture) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(activity.getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.profilePicture.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e("ERROR", "Showing round profile picture failed", e);
                        }
                    }
                });

        holder.profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MemberChatActivity.class);
                intent.putExtra("username", subscriptions.get(holder.getAdapterPosition()).getCreator().getUsername());
                intent.putExtra("club", club);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subscriptions.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        View profileLayout;
        ImageView profilePicture;
        TextView fullName, username, membershipType;

        public ItemViewHolder(View itemView) {
            super(itemView);
            profileLayout = itemView.findViewById(R.id.profile_layout);
            profilePicture = itemView.findViewById(R.id.profile_picture);
            fullName = itemView.findViewById(R.id.first_name);
            username = itemView.findViewById(R.id.username);
            membershipType = itemView.findViewById(R.id.membership_type);
        }
    }

}
