package com.lisplay;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MembershipFragment extends Fragment {
    private static final String TAG = "MembershipFragment";

    @Inject
    PreferenceManager pm;

    @Inject
    NetworkService networkService;

    private View root, noSubscriptionText;
    private ImageView storyPicture;
    private SubscriptionAdapter activeSubscriptionsAdapter;
    private List<DashboardModel.SubscribersActive> activeSubscriptionList = new ArrayList<>();

    public static MembershipFragment getInstance() {
        return new MembershipFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root != null) return root;
        root = inflater.inflate(R.layout.fragment_membership, container, false);
        final Activity activity = getActivity();
        ((MyApp) activity.getApplication()).getNetComponent().inject(this);
        noSubscriptionText = root.findViewById(R.id.no_subscriptions_text);
        storyPicture = root.findViewById(R.id.story_picture);

        RecyclerView activeSubscribersRV = root.findViewById(R.id.active_subscriptions_rv);
        activeSubscriptionsAdapter = new SubscriptionAdapter(activity, activeSubscriptionList);
        activeSubscribersRV.setLayoutManager(new LinearLayoutManager(activity));
        activeSubscribersRV.setAdapter(activeSubscriptionsAdapter);

        root.findViewById(R.id.open_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pm.isCreator())
                    startActivity(new Intent(getActivity(), ChatActivity.class));
            }
        });

        UserModel.User savedUser = new Gson().fromJson(pm.getUserObject(), new TypeToken<UserModel.User>() {
        }.getType());

        Glide.with(this)
                .load(savedUser.getPicture())
                .asBitmap()
                .centerCrop()
                .override(500, 500)
                .into(new BitmapImageViewTarget(storyPicture) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(activity.getApplicationContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            storyPicture.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e(TAG, "Showing round profile picture failed", e);
                        }
                    }
                });

        getMySubscriptions();
        return root;
    }

    private void getMySubscriptions() {
        Observable<DashboardModel> observable = networkService.getMySubscriptions(pm.getToken());
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DashboardModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
//                        loadingLayout.findViewById(R.id.tap_to_retry).setVisibility(View.VISIBLE);
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(DashboardModel model) {
                        populateDashboard(model);
//                        loadingLayout.setVisibility(View.GONE);
                    }
                });
    }

    private void populateDashboard(DashboardModel model) {
        if (model.getSubscriptionsActive().size() == 0) {
            noSubscriptionText.setVisibility(View.VISIBLE);
            return;
        }
        activeSubscriptionList.addAll(model.getSubscriptionsActive());
        activeSubscriptionsAdapter.notifyDataSetChanged();
    }
}
