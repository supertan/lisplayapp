package com.lisplay;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DashboardActivity extends AppCompatActivity {
    private static final String TAG = "DashboardActivity";

    @Inject
    PreferenceManager pm;
    @Inject
    NetworkService networkService;

    private View loadingLayout;
    private SubscribersAdapter activeSubscribersAdapter, cancelledSubscribersAdapter;
    private List<DashboardModel.SubscribersActive> activeSubscribersList = new ArrayList<>();
    private List<DashboardModel.SubscribersActive> cancelledSubscribersList = new ArrayList<>();
    private TextView activeSupporters, activeSupportersCount, cancelledSupportersCount, revenueThisMonth, supportersJoined, supportersLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_dashboard);

        loadingLayout = findViewById(R.id.loading_layout);
        activeSupporters = findViewById(R.id.active_supporters);
        revenueThisMonth = findViewById(R.id.revenue_this_month);
        supportersJoined = findViewById(R.id.supporters_joined);
        supportersLeft = findViewById(R.id.supporters_left);
        activeSupportersCount = findViewById(R.id.active_supporters_count);
        cancelledSupportersCount = findViewById(R.id.cancelled_supporters_count);

        RecyclerView activeSubscribersRV = findViewById(R.id.active_subscribers_rv);
        RecyclerView cancelledSubscribersRV = findViewById(R.id.cancelled_subscribers_rv);

        activeSubscribersAdapter = new SubscribersAdapter(this, activeSubscribersList);
        activeSubscribersRV.setLayoutManager(new LinearLayoutManager(this));
        activeSubscribersRV.setAdapter(activeSubscribersAdapter);

        cancelledSubscribersAdapter = new SubscribersAdapter(this, cancelledSubscribersList);
        cancelledSubscribersRV.setLayoutManager(new LinearLayoutManager(this));
        cancelledSubscribersRV.setAdapter(cancelledSubscribersAdapter);

        loadingLayout.findViewById(R.id.tap_to_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingLayout.findViewById(R.id.tap_to_retry).setVisibility(View.GONE);
                getDashboardData();
            }
        });

        getDashboardData();
    }


    private void getDashboardData() {
        Observable<DashboardModel> observable = networkService.getDashboardData(pm.getToken());
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DashboardModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadingLayout.findViewById(R.id.tap_to_retry).setVisibility(View.VISIBLE);
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(DashboardModel model) {
                        populateDashboard(model);
                        loadingLayout.setVisibility(View.GONE);
                    }
                });
    }

    private void populateDashboard(DashboardModel model) {
        activeSupporters.setText(String.valueOf(model.getCurrentSubscribersCount()));
        revenueThisMonth.setText(String.valueOf(model.getThisMonthRevenue()));
        supportersJoined.setText(String.valueOf(model.getJoinedThisMonth()));
        supportersLeft.setText(String.valueOf(model.getLeftThisMonth()));
        activeSupportersCount.setText(String.valueOf(model.getSubscribersActive().size()));
        cancelledSupportersCount.setText(String.valueOf(model.getSubscribersCancelled().size()));

        if (model.getSubscribersActive().size() == 0)
            findViewById(R.id.no_subscriber_text).setVisibility(View.VISIBLE);
        else {
            activeSubscribersList.addAll(model.getSubscribersActive());
            activeSubscribersAdapter.notifyDataSetChanged();
        }

        if (model.getSubscribersCancelled().size() == 0)
            findViewById(R.id.no_cancelled_subscription_text).setVisibility(View.VISIBLE);
        else {
            cancelledSubscribersList.addAll(model.getSubscribersCancelled());
            cancelledSubscribersAdapter.notifyDataSetChanged();
        }
    }
}
