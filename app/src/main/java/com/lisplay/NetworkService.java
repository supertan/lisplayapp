package com.lisplay;

import android.support.annotation.Nullable;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by tan on 27/03/17.
 **/

public interface NetworkService {

    @FormUrlEncoded
    @POST("rest-auth/login/")
    Observable<ResponseModel> loginUser(@Field("username") String username,
                                        @Field("password") String password);

    @FormUrlEncoded
    @POST("rest-auth/registration/")
    Observable<ResponseModel> signupUser(@Field("username") String username,
                                         @Field("email") String email,
                                         @Field("password1") String password1,
                                         @Field("password2") String password2);

    @FormUrlEncoded
    @POST("rest-auth/registration/verify-email/")
    Observable<ResponseModel> verifyEmail(@Field("key") String key);

    @GET("rest-auth/user/")
    Observable<ResponseModel> getUser(@Header("Authorization") String token);

    @GET("api/get_creators/")
    Observable<UserModel> getCreators(@Header("Authorization") String token);

    @GET("api/dashboard/")
    Observable<DashboardModel> getDashboardData(@Header("Authorization") String token);

    @GET("api/my_subscriptions/")
    Observable<DashboardModel> getMySubscriptions(@Header("Authorization") String token);

    @GET("api/get_firebase_token/")
    Observable<ResponseModel> getFirebaseToken(@Header("Authorization") String token,
                                               @Query("user_token") String userToken);

    @GET("api/get_user/")
    Observable<UserModel.User> getUser(@Header("Authorization") String token,
                                       @Query("username") String username);

    @FormUrlEncoded
    @POST("api/update_social_details/")
    Observable<UserModel.User> updateSocialDetails(@Header("Authorization") String token,
                                                   @FieldMap Map<String, String> data);

    @Multipart
    @POST("api/update_user/")
    Observable<UserModel.User> updateUser(@Header("Authorization") String token,
                                          @PartMap Map<String, RequestBody> data,
                                          @Nullable @Part MultipartBody.Part profilePicture,
                                          @Nullable @Part MultipartBody.Part featuredImage);

    @FormUrlEncoded
    @POST("api/update_creator/")
    Observable<UserModel.User> updateCreator(@Header("Authorization") String token,
                                             @FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/create_subscription/")
    Observable<ResponseModel> createSubscription(@Header("Authorization") String token,
                                                 @FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/subscription_authenticated/")
    Observable<ResponseModel> subscriptionAuthenticated(@Header("Authorization") String token,
                                                        @FieldMap Map<String, String> data);
}
