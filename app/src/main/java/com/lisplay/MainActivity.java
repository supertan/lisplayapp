package com.lisplay;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    @Inject
    NetworkService networkService;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout = findViewById(R.id.main_tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("EXPLORE"));
        tabLayout.addTab(tabLayout.newTab().setText("MEMBER"));
        tabLayout.addTab(tabLayout.newTab().setText("PROFILE"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.addOnTabSelectedListener(this);

        viewPager = findViewById(R.id.main_viewpager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(new MainPagerAdapter((getFragmentManager())));
    }

    public void setTab(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private class MainPagerAdapter extends FragmentPagerAdapter {
        int PAGES = 3;

        MainPagerAdapter(android.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ExploreFragment.getInstance();
                case 1:
                    return MembershipFragment.getInstance();
                case 2:
                    return ProfileFragment.getInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return PAGES;
        }
    }

}
