package com.lisplay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SplashActivity extends Activity {
    private static final String TAG = "SplashActivity";

    @Inject
    PreferenceManager pm;
    @Inject
    NetworkService networkService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_splash);

        try {
            String url = getIntent().getDataString();
            String key = url.split("confirm-email/")[1];
            if (key.substring(key.length() - 1).equals("/"))
                key = key.substring(0, key.length() - 1).trim();

            if (!key.isEmpty()) verifyEmail(key);
            else startNextActivity();

        } catch (Exception e) {
            e.printStackTrace();
            startNextActivity();
        }
    }

    private void startNextActivity() {
        switch (pm.getAppStage()) {
            case 0:
                startActivity(new Intent(this, LoginSignupActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;

            case 1:
                startActivity(new Intent(this, WelcomeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;

            case 2:
                startActivity(new Intent(this, MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                break;

            default:
                break;
        }
    }

    private void verifyEmail(String key) {
        if (!Utils.isInternetAvailable(this, true)) return;
        Observable<ResponseModel> observable = networkService.verifyEmail(key);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        HttpException error = (HttpException) e;
                        try {
                            String errorBody = error.response().errorBody().string();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                        Toast.makeText(SplashActivity.this, "Failed to confirm email. Please try again.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        if (model.getDetail().equalsIgnoreCase("ok")) {
                            Toast.makeText(SplashActivity.this, "Email verified. Please login and continue.", Toast.LENGTH_LONG).show();
                            startNextActivity();
                        }
                    }
                });
    }
}
