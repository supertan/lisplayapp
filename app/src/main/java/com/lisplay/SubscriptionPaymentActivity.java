package com.lisplay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.HashMap;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SubscriptionPaymentActivity extends Activity implements PaymentResultListener {
    private static final String TAG = "CreatorProfileActivity";

    private String subscriptionId;
    private TextView nextPayment;
    private ResponseModel subscriptionResponseModel;

    @Inject
    NetworkService networkService;
    @Inject
    PreferenceManager pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_subscription_payment);

        TextView confirmPledge = findViewById(R.id.confirm_pledge_text);
        TextView completePayment = findViewById(R.id.complete_payment_text);
        TextView todayCharge = findViewById(R.id.todays_charge);
        nextPayment = findViewById(R.id.next_payment_text);

        findViewById(R.id.pay_with_credit_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (subscriptionResponseModel != null)
                    subscriptionPaymentInit(subscriptionResponseModel);
                else
                    Utils.showToast(SubscriptionPaymentActivity.this, "Please try again!");
            }
        });

        Intent intent = getIntent();
        String creatorName = intent.getStringExtra("creator_first_name");
        String creatorUsername = intent.getStringExtra("creator_username");
        String amount = intent.getStringExtra("amount");

        String club = getClub(Integer.parseInt(amount));
        confirmPledge.setText(getString(R.string.confirm_your_pledge_to, creatorName));
        completePayment.setText(getString(R.string.complete_the_payment, creatorName, club));
        todayCharge.setText(getString(R.string.today_s_charge, amount));
        nextPayment.setText(getString(R.string.next_payment_subscription_message, amount, Utils.getSubscriptionStartDay()));

        createSubscription(amount, creatorUsername);
    }

    private String getClub(int amount) {
        if (amount < 100) return "Club 2";
        else if (amount >= 100 && amount < 1000) return "Club 3";
        else return "Club 4";
    }

    private void createSubscription(String amount, String creatorUsername) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating subscription...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("creator", creatorUsername);
        hashMap.put("amount", amount);

        Observable<ResponseModel> observable = networkService.createSubscription(pm.getToken(), hashMap);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        progressDialog.dismiss();

                        if (model.getResponseCode() == 1) {
                            Toast.makeText(SubscriptionPaymentActivity.this,
                                    "Error: " + model.getResponseMessage(),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        subscriptionId = model.getSubscriptionId();
                        subscriptionResponseModel = model;
//                        subscriptionPaymentInit(model);
                    }
                });
    }

    public void subscriptionPaymentInit(ResponseModel model) {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Checkout checkout = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Lisplay");
            options.put("description", "Subscription");
            options.put("subscription_id", model.getSubscriptionId());
            //You can omit the image option to fetch the image from dashboard
//            options.put("image", "https://lh3.googleusercontent.com/03_kq2L8zzV4AjxEz52j49gVQm4dOnws5gPC1XpWA_ZnbGHfl0M2IhkB5MJdwmTBUjM=w100");
            options.put("currency", "INR");
            options.put("amount", model.getAmount() * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", pm.getUserEmail());
            preFill.put("contact", pm.getUserMobile());
            options.put("prefill", preFill);

            checkout.open(this, options);
        } catch (Exception e) {
            Toast.makeText(this, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Log.d(TAG, "Payment Successful: " + razorpayPaymentID);
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            updateSubscriptionAuthenticated();

        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.d(TAG, "Payment failed: " + code + " " + response);
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void updateSubscriptionAuthenticated() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("subscription_id", subscriptionId);

        Observable<ResponseModel> observable = networkService.subscriptionAuthenticated(pm.getToken(), hashMap);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseModel model) {
                        if (model.getResponseCode() == 0) {
                            Toast.makeText(SubscriptionPaymentActivity.this,
                                    "Subscription authenticated",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


}
