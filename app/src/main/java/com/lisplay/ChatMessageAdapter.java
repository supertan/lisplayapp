package com.lisplay;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ChatMessageAdapter extends RecyclerView.Adapter {
    final String TAG = "ChatMessageAdapter";
    private static final int OUTGOING = 0;
    private static final int INCOMING = -1;
    private List<ChatMessageModel> chatMessageList;
    private String uid;
    private HashMap<String, String> colorMap = new HashMap<>();
    Activity activity;


    public ChatMessageAdapter(Activity activity, List<ChatMessageModel> items, String uid) {
        this.activity = activity;
        this.chatMessageList = items;
        this.uid = uid;
        addColorsToMap(this.colorMap);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == INCOMING) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chat_incoming, parent, false);
            return new ReceivedMessageHolder(view);
        }

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ChatMessageModel.TYPE.TEXT:
                view = inflater.inflate(R.layout.adapter_chat_outgoing, parent, false);
                viewHolder = new SentTextMessageHolder(view);
                break;
            case ChatMessageModel.TYPE.IMAGE:
                view = inflater.inflate(R.layout.adapter_chat_image_outgoing, parent, false);
                viewHolder = new SentImageMessageHolder(view);
                break;
            default:
                view = inflater.inflate(R.layout.adapter_chat_outgoing, parent, false);
                viewHolder = new SentTextMessageHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChatMessageModel message = chatMessageList.get(position);

        switch (holder.getItemViewType()) {
            case INCOMING:
                ((ReceivedMessageHolder) holder).bind(message);
                break;
            case ChatMessageModel.TYPE.TEXT:
                ((SentTextMessageHolder) holder).bind(message);
                break;
            case ChatMessageModel.TYPE.IMAGE:
                ((SentImageMessageHolder) holder).bind(message);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (!chatMessageList.get(position).getSenderUid().equalsIgnoreCase(uid))
            return INCOMING;
        return chatMessageList.get(position).getMessageType();
    }

    private void addColorsToMap(HashMap<String, String> map) {
        map.put("a", "#0271a4");
        map.put("b", "#0d7055");
        map.put("c", "#64410c");
        map.put("d", "#8400ff");
        map.put("e", "#71032f");
        map.put("f", "#058469");
        map.put("g", "#519e06");
        map.put("h", "#c15405");
        map.put("i", "#af370c");
        map.put("j", "#104ab1");
        map.put("k", "#a90f6e");
        map.put("l", "#0015ff");
        map.put("m", "#0a8c8c");
        map.put("n", "#844905");
        map.put("o", "#069606");
        map.put("p", "#840784");
        map.put("q", "#609700");
        map.put("r", "#9b5603");
        map.put("s", "#4900e4");
        map.put("t", "#9b1f11");
        map.put("u", "#6b2108");
        map.put("v", "#075400");
        map.put("w", "#15047e");
        map.put("x", "#085656");
        map.put("y", "#074243");
        map.put("z", "#420a59");
    }

    private class SentTextMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;
        LinearLayout chatLayout;

        SentTextMessageHolder(View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.chat_message);
            timeText = itemView.findViewById(R.id.chat_message_time);
            chatLayout = itemView.findViewById(R.id.chat_layout);
        }

        void bind(ChatMessageModel message) {
            messageText.setText(message.getMessageText());
            timeText.setText(Utils.getChatDate(message.getMessageTime()));
        }
    }

    private class SentImageMessageHolder extends RecyclerView.ViewHolder {
        TextView timeText;
        ImageView chatImage;
        FrameLayout chatImageLayout;
        ProgressBar progressBar;

        SentImageMessageHolder(View itemView) {
            super(itemView);
            chatImage = itemView.findViewById(R.id.chat_image);
            timeText = itemView.findViewById(R.id.chat_message_time);
            progressBar = itemView.findViewById(R.id.progress_bar);
            chatImageLayout = itemView.findViewById(R.id.chat_image_layout);
        }

        void bind(ChatMessageModel message) {
            timeText.setText(Utils.getChatDate(message.getMessageTime()));
            if (message.getMessageTime() == null) progressBar.setVisibility(View.VISIBLE);
            else progressBar.setVisibility(View.GONE);
            Glide.with(activity)
                    .load(message.getMediaUrl())
                    .override(200, 200)
                    .centerCrop()
                    .into(chatImage);
        }
    }

    public void updateMessage(ChatMessageModel messageModel) {
        int size = getItemCount() - 1;
        for (int i = size; i > 0; i--) {
            if (chatMessageList.get(i).equals(messageModel)) {
                chatMessageList.get(i).setMessageTime(new Date(System.currentTimeMillis()));
                notifyItemChanged(i);
            }
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        LinearLayout chatLayout;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.chat_message);
            timeText = itemView.findViewById(R.id.chat_message_time);
            nameText = itemView.findViewById(R.id.user_name);
            chatLayout = itemView.findViewById(R.id.chat_layout);
        }

        void bind(ChatMessageModel message) {
            messageText.setText(message.getMessageText());
            timeText.setText(Utils.getChatDate(message.getMessageTime()));
            nameText.setText(message.getMessageUser());

            try {
                String name = message.getMessageUser();
                String first = name.substring(0, 1).toLowerCase();
                String color = colorMap.get(first);
                nameText.setTextColor(Color.parseColor(color));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

