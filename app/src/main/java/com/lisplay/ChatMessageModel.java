package com.lisplay;

import android.support.annotation.Keep;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.UUID;

@Keep
public class ChatMessageModel {

    private String messageText;
    private String mediaUrl;
    private String messageUser; // sender's full name
    private String senderUid; // sender's username
    private int messageType;
    @ServerTimestamp
    private Date messageTime;

    public ChatMessageModel(String messageText, String messageUser, String senderUid, int messageType) {
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.senderUid = senderUid;
        this.messageType = messageType;

        // Firebase wil replace null with current time
        messageTime = null;
    }

    public ChatMessageModel(String mediaUrl, String messageText, String messageUser, String senderUid, int messageType) {
        this.mediaUrl = mediaUrl;
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.senderUid = senderUid;
        this.messageType = messageType;

        // Firebase wil replace null with current time
        messageTime = null;
    }

    public ChatMessageModel() {

    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Date messageTime) {
        this.messageTime = messageTime;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public static class TYPE {
        public static final int TEXT = 1;
        public static final int IMAGE = 2;
        public static final int VIDEO = 3;
    }
}