package com.lisplay;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ExploreFragment extends Fragment {
    private static final String TAG = "ExploreFragment";

    private View root, loadingLayout;
    private CreatorProfileAdapter creatorProfileAdapter;
    private List<UserModel.User> creators = new ArrayList<>();

    @Inject
    NetworkService networkService;
    @Inject
    PreferenceManager pm;

    public static ExploreFragment getInstance() {
        return new ExploreFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root != null) return root;
        root = inflater.inflate(R.layout.fragment_explore, container, false);
        Activity activity = getActivity();
        ((MyApp) activity.getApplication()).getNetComponent().inject(this);

        loadingLayout = root.findViewById(R.id.loading_layout);
        RecyclerView playlistsRV = root.findViewById(R.id.explore_creators_rv);
        creatorProfileAdapter = new CreatorProfileAdapter(activity, creators);
        GridLayoutManager playlistLayoutManager = new GridLayoutManager(activity, 2);
        playlistsRV.setLayoutManager(playlistLayoutManager);
        playlistsRV.setAdapter(creatorProfileAdapter);

        loadingLayout.findViewById(R.id.tap_to_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingLayout.findViewById(R.id.tap_to_retry).setVisibility(View.GONE);
                getCreators();
            }
        });

        getCreators();
        return root;
    }

    private void getCreators() {
        Observable<UserModel> observable = networkService.getCreators(pm.getToken());
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadingLayout.findViewById(R.id.tap_to_retry).setVisibility(View.VISIBLE);
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(UserModel model) {
                        creators.addAll(model.getUsers());
                        creatorProfileAdapter.notifyDataSetChanged();
                        loadingLayout.setVisibility(View.GONE);
                    }
                });
    }
}

