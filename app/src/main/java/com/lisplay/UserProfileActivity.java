package com.lisplay;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UserProfileActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {
    private static final String TAG = "CreatorProfileActivity";
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;

    private ImageView featuredImage, profilePicture;
    private TextView fullName, shortBio, profileDescription;
    private TextView icFacebook, icTwitter, icInstagram, icYoutube, icGithub, icWebsite, icOther;
    private int mMaxScrollSize;
    private boolean mIsAvatarShown = true;

    @Inject
    NetworkService networkService;
    @Inject
    PreferenceManager pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getApplication()).getNetComponent().inject(this);
        setContentView(R.layout.activity_user_profile);
        Typeface fontAwesome = Typeface.createFromAsset(getAssets(), "fonts/fa-brands-400.ttf");
        icFacebook = findViewById(R.id.icon_facebook);
        icTwitter = findViewById(R.id.icon_twitter);
        icInstagram = findViewById(R.id.icon_instagram);
        icYoutube = findViewById(R.id.icon_youtube);
        icGithub = findViewById(R.id.icon_github);
        icWebsite = findViewById(R.id.icon_website);
        icOther = findViewById(R.id.icon_other);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null)
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appBarLayout.getTotalScrollRange();

        fullName = findViewById(R.id.first_name);
        shortBio = findViewById(R.id.short_bio);
        profileDescription = findViewById(R.id.profile_description);
        featuredImage = findViewById(R.id.featured_image);
        profilePicture = findViewById(R.id.profile_picture);
        icFacebook.setTypeface(fontAwesome);
        icTwitter.setTypeface(fontAwesome);
        icInstagram.setTypeface(fontAwesome);
        icYoutube.setTypeface(fontAwesome);
        icGithub.setTypeface(fontAwesome);
        icWebsite.setTypeface(fontAwesome);
        icOther.setTypeface(fontAwesome);

        String username = getIntent().getStringExtra("username");
        ((TextView) findViewById(R.id.username)).setText(String.format("@%s", username));
        getUser(username);
    }

    private void getUser(String username) {
        Observable<UserModel.User> observable = networkService.getUser(pm.getToken(), username);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserModel.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onNext(UserModel.User model) {
                        populateUserData(model);
                    }
                });
    }

    private void populateUserData(UserModel.User user) {
        fullName.setText(user.getFirstName());
        shortBio.setText(user.getShortBio());
        profileDescription.setText(user.getProfileDescription());
        populateSocialLinks(user.getSocialLinks());

        Glide.with(this)
                .load(user.getFeaturedImage())
                .centerCrop()
                .into(featuredImage);

        Glide.with(this)
                .load(user.getPicture())
                .asBitmap()
                .centerCrop()
                .override(500, 500)
                .into(new BitmapImageViewTarget(profilePicture) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                    .create(UserProfileActivity.this.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profilePicture.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            Log.e(TAG, "Showing round profile picture failed", e);
                        }
                    }
                });
    }

    private void populateSocialLinks(String socialLinks) {
        if (socialLinks == null || socialLinks.isEmpty()) {
            findViewById(R.id.social_media).setVisibility(View.GONE);
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(socialLinks);

            final String facebookLink = jsonObject.get("facebook_link").toString();
            if (!facebookLink.isEmpty()) {
                icFacebook.setVisibility(View.VISIBLE);
                icFacebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookLink));
                        startActivity(intent);
                    }
                });
            }

            final String twitterLink = jsonObject.get("twitter_link").toString();
            if (!twitterLink.isEmpty()) {
                icTwitter.setVisibility(View.VISIBLE);
                icTwitter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterLink));
                        startActivity(intent);
                    }
                });
            }

            final String instagramLink = jsonObject.get("instagram_link").toString();
            if (!instagramLink.isEmpty()) {
                icInstagram.setVisibility(View.VISIBLE);
                icInstagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(instagramLink));
                        startActivity(intent);
                    }
                });
            }

            final String youtubeLink = jsonObject.get("youtube_link").toString();
            if (!youtubeLink.isEmpty()) {
                icYoutube.setVisibility(View.VISIBLE);
                icYoutube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink));
                        startActivity(intent);
                    }
                });
            }

            final String githubLink = jsonObject.get("github_link").toString();
            if (!githubLink.isEmpty()) {
                icGithub.setVisibility(View.VISIBLE);
                icGithub.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(githubLink));
                        startActivity(intent);
                    }
                });
            }

            final String websiteLink = jsonObject.get("website_link").toString();
            if (!websiteLink.isEmpty()) {
                icWebsite.setVisibility(View.VISIBLE);
                icWebsite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(websiteLink));
                        startActivity(intent);
                    }
                });
            }

            final String otherLink = jsonObject.get("other_link").toString();
            if (!otherLink.isEmpty()) {
                icOther.setVisibility(View.VISIBLE);
                icOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(otherLink));
                        startActivity(intent);
                    }
                });
            }

            Log.d(TAG, jsonObject.get("website_link").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(verticalOffset)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            profilePicture.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            profilePicture.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }

    }
}

